"""
file utilities
"""

import os
import re


def file_search_and_replace_re(in_out_file, replace_map):
    """
    search and replace in file, inplace, using regex
    :param in_out_file: path to the file to replace into, will be overwritten
    :param replace_map: a dictionary with key=seek regex, value=replace regex
    """

    # read input
    fin = open(in_out_file, 'r')
    content = fin.read()

    # seek & replace each entry in map
    for seek, replace in replace_map.items():
        replaced = re.sub(seek, replace, content, flags=re.M)
        content = replaced

    # write output to tmp file
    tmp_path = '%s.tmp' % in_out_file
    fout = open(tmp_path, 'w')
    fout.write(content)
    fout.close()
    fin.close()

    # replace out with tmp
    os.remove(in_out_file)
    os.rename(tmp_path, in_out_file)
