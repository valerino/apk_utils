"""
apk utilities

requirements:
pip3 install whichcraft
jarsigner, apktool in path
"""
import os
import shutil
import sys

import subprocess
import whichcraft

__JARSIGNER_BIN = ''
__APKTOOL_BIN = ''
__ZIPALIGN_BIN = ''


def apk_checktools():
    """
    check tools needed by this lib
    """
    global __JARSIGNER_BIN
    global __APKTOOL_BIN
    global __ZIPALIGN_BIN

    # jarsigner
    js = whichcraft.which('jarsigner')
    if js is None:
        raise Exception('[!] jarsigner must be in path!')
    __JARSIGNER_BIN = js

    # apktool, search both apktool.sh and apktool (one or the other is ok)
    ap = whichcraft.which('apktool')
    if ap is None:
        ap = whichcraft.which('apktool.sh')
        if ap is None:
            raise Exception('[!] apktool or apktool.sh must be in path!')

    __APKTOOL_BIN = ap

    # zipalign
    zp = whichcraft.which('zipalign')
    if zp is None:
        raise Exception('[!] zipalign must be in path!')

    __ZIPALIGN_BIN = zp


def apk_recompile(decompiled_path, out_apk):
    """
    recompile the apk with apktool
    """
    print('[-] recompiling from %s using apktool, target=%s' %
          (decompiled_path, out_apk))
    if os.path.exists(out_apk):
        # delete first
        os.remove(out_apk)

    # recompile
    cmdline = [__APKTOOL_BIN, 'b', decompiled_path, '-o', out_apk]
    res = subprocess.call(cmdline)
    if res != 0:
        raise Exception(
            '[!] recompiling "%s" failed: %s' % ('%s b %s -o %s' % (__APKTOOL_BIN, decompiled_path, out_apk), str(res)))


def apk_decompile(in_apk, decompiled_path, decode_resources=True):
    """
    decompile the apk with apktool
    if decode_resources is False, resources are not decompiled
    """

    print('[-] decompiling %s using apktool, target=%s, decode_resources=%r' %
          (in_apk, decompiled_path, decode_resources))
    if decode_resources == False:
        cmdline = [__APKTOOL_BIN, 'd', in_apk, '-r', '-o', decompiled_path]
    else:
        # default, decode resources
        cmdline = [__APKTOOL_BIN, 'd', in_apk, '-o', decompiled_path]

    res = subprocess.call(cmdline)
    if res != 0:
        raise Exception('[!] decompiling %s failed!' % in_apk)


def apk_resign(apk_path, ks, kn, kp, ksp):
    """
    resign the given apk with jarsigner
    : param ks: keystore path
    : param kn: key alias
    : param kp: key password
    : param ksp: keystore password
    : return:
    """
    # resign
    print('\t[-] resigning %s with jarsigner, keystore=%s, keyname=%s, keypass=%s, storepass=%s' % (apk_path,
                                                                                                    ks, kn, kp, ksp))
    cmdline = [__JARSIGNER_BIN, '-verbose', '-digestalg', 'SHA1', '-keystore',
               ks, '-storepass', ksp, '-keypass', kp, apk_path, kn]
    res = subprocess.call(cmdline)

    if res != 0:
        raise Exception('[!] resigning %s failed!' % apk_path)


def apk_zipalign(apk_in):
    """
    zipalign apk, in place (using a temporary file)
    """
    # resign
    in_size = os.stat(apk_in).st_size
    print('\t[-] zipaligning apk=%s, unaligned size=%d' % (apk_in, in_size))
    out_tmp = apk_in + '.tmp'
    cmdline = [__ZIPALIGN_BIN, '-f', '4', apk_in, out_tmp]
    res = subprocess.call(cmdline)
    if res != 0:
        raise Exception('[!] zipalign %s failed!' % apk_in)

    # copy back
    shutil.copyfile(out_tmp, apk_in)
    in_size = os.stat(apk_in).st_size
    print('\t[-] zipaligned apk=%s, aligned size=%d' % (apk_in, in_size))
    os.remove(out_tmp)
